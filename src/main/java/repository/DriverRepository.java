package repository;

import entity.Driver;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface DriverRepository extends CrudRepository<Driver, Integer> {

}
