package repository;

import entity.Fine;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


@Transactional
public interface FineRepository extends CrudRepository<Fine, Integer> {

    @Query("SELECT f FROM Fine f WHERE f.decreeNumber = :decreeNumber")
    public Fine findFineByDecreeNumber(@Param("decreeNumber") String decreeNumber);

}
