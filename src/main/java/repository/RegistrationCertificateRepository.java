package repository;

import entity.RegistrationCertificate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface RegistrationCertificateRepository extends CrudRepository<RegistrationCertificate, Integer> {

    @Query("SELECT r FROM RegistrationCertificate r WHERE r.vin = :VIN")
    public RegistrationCertificate findRegistrationSertificateByVin(@Param("VIN") String VIN);
}
