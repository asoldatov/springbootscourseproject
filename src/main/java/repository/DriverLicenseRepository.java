package repository;

import entity.DriverLicense;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface DriverLicenseRepository extends CrudRepository<DriverLicense, Integer> {

    @Query("SELECT d FROM DriverLicense d WHERE d.series = :series AND d.number = :number")
    public DriverLicense findDriverLicenseBySeriesAndNumber(@Param("series") String series, @Param("number") Integer number);
}
