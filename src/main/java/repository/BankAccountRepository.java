package repository;

import entity.BankAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface BankAccountRepository extends CrudRepository<BankAccount, Integer> {

}
