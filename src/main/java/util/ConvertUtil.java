package util;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ConvertUtil {

    public static XMLGregorianCalendar convertDateToXMLGregorianCalendar(Date date){
        DatatypeFactory df = null;
        try {
            df = DatatypeFactory.newInstance();
            if (date != null) {
                GregorianCalendar gc = new GregorianCalendar();
                gc.setTimeInMillis(date.getTime());
                return df.newXMLGregorianCalendar(gc);
            }
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date convertXMLGregorianCalendarToDate(XMLGregorianCalendar calendar){
        if(calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }

    public static int calculateNumberOfWeekendsInRange(Date startDate, Date endDate) {
        LocalDate startLocalDate = new LocalDate(startDate);
        LocalDate endLocalDate = new LocalDate(endDate);
        return Days.daysBetween(startLocalDate, endLocalDate).getDays();
    }
}
