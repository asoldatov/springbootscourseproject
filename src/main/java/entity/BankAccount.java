package entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Table(name="bank_account")
public class BankAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="account_number")
	private String accountNumber;

	private BigDecimal balance;

	@Column(name="bank_id")
	private int bankId;

	@OneToOne
	private Driver driver;

	public BankAccount() {
	}

	public void setParametersFromModelBankAccount(model.BankAccount bankAccount){
		this.id = bankAccount.getId();
		this.accountNumber = bankAccount.getAccountNumber();
		this.balance = new BigDecimal(bankAccount.getBalance());
		this.bankId = bankAccount.getBankId();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return this.balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public int getBankId() {
		return this.bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

}