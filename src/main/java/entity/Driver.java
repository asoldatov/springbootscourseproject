package entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Driver implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="birth_place")
	private String birthPlace;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_birth")
	private Date dateOfBirth;

	@Column(name="first_name")
	private String firstName;

	@Column(name="last_name")
	private String lastName;

	@Column(name="middle_name")
	private String middleName;

	@Column(name="passport_number")
	private int passportNumber;

	@Column(name="passport_series")
	private int passportSeries;

	@OneToOne(fetch = FetchType.EAGER, mappedBy="driver", cascade = CascadeType.ALL)
	private BankAccount bankAccount;

	@OneToOne(fetch = FetchType.EAGER,  mappedBy="driver", cascade = CascadeType.ALL)
	private DriverLicense driverLicense;

	@OneToMany(fetch = FetchType.EAGER, mappedBy="driver", cascade = CascadeType.ALL)
	private List<Fine> fines;

	@OneToOne(fetch = FetchType.EAGER, mappedBy="driver", cascade = CascadeType.ALL)
	private RegistrationCertificate registrationCertificate;

	public Driver() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBirthPlace() {
		return this.birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public int getPassportNumber() {
		return this.passportNumber;
	}

	public void setPassportNumber(int passportNumber) {
		this.passportNumber = passportNumber;
	}

	public int getPassportSeries() {
		return this.passportSeries;
	}

	public void setPassportSeries(int passportSeries) {
		this.passportSeries = passportSeries;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	public DriverLicense getDriverLicense() {
		return driverLicense;
	}

	public void setDriverLicense(DriverLicense driverLicense) {
		this.driverLicense = driverLicense;
	}

	public List<Fine> getFines() {
		return this.fines;
	}

	public void setFines(List<Fine> fines) {
		this.fines = fines;
	}

	public RegistrationCertificate getRegistrationCertificate() {
		return registrationCertificate;
	}

	public void setRegistrationCertificate(RegistrationCertificate registrationCertificate) {
		this.registrationCertificate = registrationCertificate;
	}
}