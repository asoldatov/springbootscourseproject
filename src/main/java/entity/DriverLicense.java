package entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="driver_license")
public class DriverLicense implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String categories;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_issue")
	private Date dateOfIssue;

	@Column(name="gibdd_code")
	private String gibddCode;

	private int number;

	private String series;

	@Column(name="special_notes")
	private String specialNotes;

	@Temporal(TemporalType.DATE)
	@Column(name="valid_until")
	private Date validUntil;

	@OneToOne
	private Driver driver;

	public DriverLicense() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategories() {
		return this.categories;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	public Date getDateOfIssue() {
		return this.dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getGibddCode() {
		return this.gibddCode;
	}

	public void setGibddCode(String gibddCode) {
		this.gibddCode = gibddCode;
	}

	public int getNumber() {
		return this.number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getSeries() {
		return this.series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getSpecialNotes() {
		return this.specialNotes;
	}

	public void setSpecialNotes(String specialNotes) {
		this.specialNotes = specialNotes;
	}

	public Date getValidUntil() {
		return this.validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

}