package entity;

import util.ConvertUtil;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

@Entity
public class Fine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="article_number")
	private String articleNumber;

	@Column(name="body_number")
	private String bodyNumber;

	@Column(name="car_mark")
	private String carMark;

	@Column(name="car_model")
	private String carModel;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="datetime_of_fine")
	private Date datetimeOfFine;

	@Column(name="decree_number")
	private String decreeNumber;

	@Column(name="fine_sum")
	private int fineSum;

	@Column(name="gibdd_number")
	private String gibddNumber;

	@Column(name="inspector_department")
	private String inspectorDepartment;

	@Column(name="inspector_fio")
	private String inspectorFio;

	@Column(name="motor_number")
	private String motorNumber;

	private int phonenumber;

	@Column(name="place_of_fine")
	private String placeOfFine;

	@Column(name="resident_at")
	private String residentAt;

	@Column(name="state_sign")
	private String stateSign;

	private byte status;

	private String vin;

	@Column(name="work_at")
	private String workAt;

	@Column(name="work_phonenumber")
	private int workPhonenumber;

	@Column(name="pay_sum")
	private int paySum;

	@ManyToOne
	private Driver driver;

	public Fine() {
	}

	public void setParametersFromModelFine(model.Fine fine) {
		this.articleNumber = fine.getArticleNumber();
		this.bodyNumber = fine.getBodyNumber();
		this.carMark = fine.getCarMark();
		this.carModel = fine.getCarModel();
		this.datetimeOfFine = ConvertUtil.convertXMLGregorianCalendarToDate(fine.getDatetimeOfFine());
		this.decreeNumber = fine.getDecreeNumber();
		this.fineSum = fine.getFineSum();
		this.gibddNumber = fine.getGibddNumber();
		this.inspectorDepartment = fine.getInspectorDepartment();
		this.inspectorFio = fine.getInspectorFio();
		this.motorNumber = fine.getMotorNumber();
		this.phonenumber = fine.getPhonenumber();
		this.placeOfFine = fine.getPlaceOfFine();
		this.residentAt = fine.getResidentAt();
		this.stateSign = fine.getStateSign();
		this.status = fine.getStatus();
		this.vin = fine.getVin();
		this.workAt = fine.getVin();
		this.workPhonenumber = fine.getWorkPhonenumber();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArticleNumber() {
		return this.articleNumber;
	}

	public void setArticleNumber(String articleNumber) {
		this.articleNumber = articleNumber;
	}

	public String getBodyNumber() {
		return this.bodyNumber;
	}

	public void setBodyNumber(String bodyNumber) {
		this.bodyNumber = bodyNumber;
	}

	public String getCarMark() {
		return this.carMark;
	}

	public void setCarMark(String carMark) {
		this.carMark = carMark;
	}

	public String getCarModel() {
		return this.carModel;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	public Date getDatetimeOfFine() {
		return this.datetimeOfFine;
	}

	public void setDatetimeOfFine(Date datetimeOfFine) {
		this.datetimeOfFine = datetimeOfFine;
	}

	public String getDecreeNumber() {
		return this.decreeNumber;
	}

	public void setDecreeNumber(String decreeNumber) {
		this.decreeNumber = decreeNumber;
	}

	public int getFineSum() {
		return this.fineSum;
	}

	public void setFineSum(int fineSum) {
		this.fineSum = fineSum;
	}

	public String getGibddNumber() {
		return this.gibddNumber;
	}

	public void setGibddNumber(String gibddNumber) {
		this.gibddNumber = gibddNumber;
	}

	public String getInspectorDepartment() {
		return this.inspectorDepartment;
	}

	public void setInspectorDepartment(String inspectorDepartment) {
		this.inspectorDepartment = inspectorDepartment;
	}

	public String getInspectorFio() {
		return this.inspectorFio;
	}

	public void setInspectorFio(String inspectorFio) {
		this.inspectorFio = inspectorFio;
	}

	public String getMotorNumber() {
		return this.motorNumber;
	}

	public void setMotorNumber(String motorNumber) {
		this.motorNumber = motorNumber;
	}

	public int getPhonenumber() {
		return this.phonenumber;
	}

	public void setPhonenumber(int phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getPlaceOfFine() {
		return this.placeOfFine;
	}

	public void setPlaceOfFine(String placeOfFine) {
		this.placeOfFine = placeOfFine;
	}

	public String getResidentAt() {
		return this.residentAt;
	}

	public void setResidentAt(String residentAt) {
		this.residentAt = residentAt;
	}

	public String getStateSign() {
		return this.stateSign;
	}

	public void setStateSign(String stateSign) {
		this.stateSign = stateSign;
	}

	public byte getStatus() {
		return this.status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getVin() {
		return this.vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getWorkAt() {
		return this.workAt;
	}

	public void setWorkAt(String workAt) {
		this.workAt = workAt;
	}

	public int getWorkPhonenumber() {
		return this.workPhonenumber;
	}

	public void setWorkPhonenumber(int workPhonenumber) {
		this.workPhonenumber = workPhonenumber;
	}

	public int getPaySum() {
		return paySum;
	}

	public void setPaySum(int paySum) {
		this.paySum = paySum;
	}

	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

}