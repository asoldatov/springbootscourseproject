package entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="registration_certificate")
public class RegistrationCertificate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="body_number")
	private String bodyNumber;

	private String category;

	@Column(name="chassis_number")
	private String chassisNumber;

	private String color;

	@Column(name="engine_displacement_volume")
	private int engineDisplacementVolume;

	@Column(name="engine_power")
	private String enginePower;

	private String mark;

	private String model;

	@Column(name="motor_model")
	private String motorModel;

	@Column(name="motor_number")
	private String motorNumber;

	@Column(name="noload_weight")
	private int noloadWeight;

	@Column(name="permissible_weight")
	private int permissibleWeight;

	@Column(name="register_sign")
	private String registerSign;

	private String vin;

	@Column(name="year_of_issue")
	private int yearOfIssue;

	@OneToOne
	private Driver driver;

	public RegistrationCertificate() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBodyNumber() {
		return this.bodyNumber;
	}

	public void setBodyNumber(String bodyNumber) {
		this.bodyNumber = bodyNumber;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getChassisNumber() {
		return this.chassisNumber;
	}

	public void setChassisNumber(String chassisNumber) {
		this.chassisNumber = chassisNumber;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getEngineDisplacementVolume() {
		return this.engineDisplacementVolume;
	}

	public void setEngineDisplacementVolume(int engineDisplacementVolume) {
		this.engineDisplacementVolume = engineDisplacementVolume;
	}

	public String getEnginePower() {
		return this.enginePower;
	}

	public void setEnginePower(String enginePower) {
		this.enginePower = enginePower;
	}

	public String getMark() {
		return this.mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMotorModel() {
		return this.motorModel;
	}

	public void setMotorModel(String motorModel) {
		this.motorModel = motorModel;
	}

	public String getMotorNumber() {
		return this.motorNumber;
	}

	public void setMotorNumber(String motorNumber) {
		this.motorNumber = motorNumber;
	}

	public int getNoloadWeight() {
		return this.noloadWeight;
	}

	public void setNoloadWeight(int noloadWeight) {
		this.noloadWeight = noloadWeight;
	}

	public int getPermissibleWeight() {
		return this.permissibleWeight;
	}

	public void setPermissibleWeight(int permissibleWeight) {
		this.permissibleWeight = permissibleWeight;
	}

	public String getRegisterSign() {
		return this.registerSign;
	}

	public void setRegisterSign(String registerSign) {
		this.registerSign = registerSign;
	}

	public String getVin() {
		return this.vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public int getYearOfIssue() {
		return this.yearOfIssue;
	}

	public void setYearOfIssue(int yearOfIssue) {
		this.yearOfIssue = yearOfIssue;
	}

	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

}