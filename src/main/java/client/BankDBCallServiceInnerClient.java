package client;

import endpoint.BankDBCallServiceEndpoint;
import model.DebitRequest;
import model.DebitResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BankDBCallServiceInnerClient {

    private BankDBCallServiceEndpoint bankDBCallServiceEndpoint;

    public model.BankAccount callDebit(model.BankAccount bankAccount, Integer fineSum){
        DebitRequest request = new DebitRequest();
        request.setBankAccount(bankAccount);
        request.setSum(fineSum);
        DebitResponse response = bankDBCallServiceEndpoint.debit(request);
        return response.getBankAccount();
    }

    @Autowired
    public void setBankDBCallServiceEndpoint(BankDBCallServiceEndpoint bankDBCallServiceEndpoint) {
        this.bankDBCallServiceEndpoint = bankDBCallServiceEndpoint;
    }
}
