package client;

import endpoint.FineDBCallServiceEndpoint;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FineDBCallServiceInnerClient {

    private FineDBCallServiceEndpoint fineDBCallServiceEndpoint;

    public List<model.Fine> callGetFinesFromDBByDriverData(DriverData driverData){
        GetFinesFromDBByDriverDataRequest request = new GetFinesFromDBByDriverDataRequest();
        request.setDriverData(driverData);
        GetFinesFromDBByDriverDataResponse response = fineDBCallServiceEndpoint.getFinesFromDBByDriverData(request);
        return response.getFine();
    }

    public List<model.Fine> callGetFinesFromDBByVIN(String VIN){
        GetFinesFromDBByVINRequest request = new GetFinesFromDBByVINRequest();
        request.setVin(VIN);
        GetFinesFromDBByVINResponse response = fineDBCallServiceEndpoint.getFinesFromDBByVIN(request);
        return response.getFine();
    }

    public List<model.Fine> callGetFinesFromDBByDecreeNumber(String decreeNumber){
        GetFinesFromDBByDecreeNumberRequest request = new GetFinesFromDBByDecreeNumberRequest();
        request.setDecreeNumber(decreeNumber);
        GetFinesFromDBByDecreeNumberResponse response = fineDBCallServiceEndpoint.getFinesFromDBByDecreeNumber(request);
        return response.getFine();
    }

    @Autowired
    public void setFineDBCallServiceEndpoint(FineDBCallServiceEndpoint fineDBCallServiceEndpoint) {
        this.fineDBCallServiceEndpoint = fineDBCallServiceEndpoint;
    }
}
