package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for bankAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bankAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="bank_id" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="account_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="balance" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "http://bankDBCallService", name = "bankAccount", propOrder = {
    "id",
    "bankId",
    "accountNumber",
    "balance"
})
public class BankAccount {

    @XmlElement(required = true)
    protected Integer id;
    @XmlElement(name = "bank_id", required = true)
    protected Integer bankId;
    @XmlElement(name = "account_number", required = true)
    protected String accountNumber;
    @XmlElement(name = "balance", required = true)
    protected long balance;

    public BankAccount() {
    }

    public BankAccount(Integer id, Integer bankId, String accountNumber, long balance) {
        this.id = id;
        this.bankId = bankId;
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public BankAccount(entity.BankAccount entityBankAccount){
        this(entityBankAccount.getId(), entityBankAccount.getBankId(), entityBankAccount.getAccountNumber(), entityBankAccount.getBalance().longValue());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer value) {
        this.id = value;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer value) {
        this.bankId = value;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long value) {
        this.balance = value;
    }
}
