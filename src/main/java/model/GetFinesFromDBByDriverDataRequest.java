package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "getFinesFromDBByDriverDataRequest", namespace = "http://fineDBCallService")
public class GetFinesFromDBByDriverDataRequest {

    @XmlElement(required = true)
    protected DriverData driverData;

    public DriverData getDriverData() {
        return driverData;
    }

    public void setDriverData(DriverData driverData) {
        this.driverData = driverData;
    }
}
