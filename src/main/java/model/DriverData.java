package model;

import model.searchfineservice.SearchFineServiceRequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;


/**
 * <p>Java class for driverData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="driverData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="series" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "driverData", propOrder = {
    "series",
    "number"
})
public class DriverData {

    @XmlElement(required = true)
    protected String series;
    @XmlElement(required = true)
    protected Integer number;

    public DriverData(){}

    public DriverData(SearchFineServiceRequest.DriverData driverData){
        this.series = driverData.getSeries();
        this.number = driverData.getNumber();
    }
    public String getSeries() {
        return series;
    }

    public void setSeries(String value) {
        this.series = value;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer value) {
        this.number = value;
    }
}
