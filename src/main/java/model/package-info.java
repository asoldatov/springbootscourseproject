
@javax.xml.bind.annotation.XmlSchema(
        namespace = "http://fineDBCallService",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
        xmlns={
            @XmlNs(prefix="fineDBCall", namespaceURI="http://fineDBCallService"),
            @XmlNs(prefix = "bankDBCall", namespaceURI = "http://bankDBCallService")
        })
package model;

import javax.xml.bind.annotation.XmlNs;