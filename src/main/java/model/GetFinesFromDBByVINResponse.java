package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "getFinesFromDBByVINResponse", namespace = "http://fineDBCallService")
public class GetFinesFromDBByVINResponse {

    @XmlElement(required = true)
    protected List<Fine> fine;

    public List<Fine> getFine() {
        if (fine == null) {
            fine = new ArrayList<Fine>();
        }
        return this.fine;
    }

    public void setFine(List<Fine> fine) {
        this.fine = fine;
    }

}
