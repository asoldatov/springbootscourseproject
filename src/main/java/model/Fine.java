package model;

import util.ConvertUtil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for fine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="decreeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="datetimeOfFine" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="placeOfFine" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="inspectorDepartment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="inspectorFio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="carMark" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="carModel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stateSign" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="bodyNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="motorNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="gibddNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="articleNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="residentAt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="phonenumber" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="workAt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="workPhonenumber" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="fineSum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fine", propOrder = {
    "id",
    "decreeNumber",
    "datetimeOfFine",
    "placeOfFine",
    "inspectorDepartment",
    "inspectorFio",
    "carMark",
    "carModel",
    "stateSign",
    "vin",
    "bodyNumber",
    "motorNumber",
    "gibddNumber",
    "articleNumber",
    "residentAt",
    "phonenumber",
    "workAt",
    "workPhonenumber",
    "fineSum",
    "status"
})
public class Fine {

    @XmlElement(required = true)
    protected Integer id;
    @XmlElement(required = true)
    protected String decreeNumber;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datetimeOfFine;
    @XmlElement(required = true)
    protected String placeOfFine;
    @XmlElement(required = true)
    protected String inspectorDepartment;
    @XmlElement(required = true)
    protected String inspectorFio;
    @XmlElement(required = true)
    protected String carMark;
    @XmlElement(required = true)
    protected String carModel;
    @XmlElement(required = true)
    protected String stateSign;
    @XmlElement(name = "VIN", required = true)
    protected String vin;
    @XmlElement(required = true)
    protected String bodyNumber;
    @XmlElement(required = true)
    protected String motorNumber;
    @XmlElement(required = true)
    protected String gibddNumber;
    @XmlElement(required = true)
    protected String articleNumber;
    @XmlElement(required = true)
    protected String residentAt;
    @XmlElement(required = true)
    protected Integer phonenumber;
    @XmlElement(required = true)
    protected String workAt;
    @XmlElement(required = true)
    protected Integer workPhonenumber;
    @XmlElement(required = true)
    protected Integer fineSum;
    @XmlElement(required = true)
    protected byte status;

    public Fine(){
    }

    public Fine(Integer id, String decreeNumber, XMLGregorianCalendar datetimeOfFine, String placeOfFine,
                String inspectorDepartment, String inspectorFio, String carMark, String carModel, String stateSign,
                String vin, String bodyNumber, String motorNumber, String gibddNumber, String articleNumber,
                String residentAt, Integer phonenumber, String workAt, Integer workPhonenumber,
                Integer fineSum, byte status) {
        this.id = id;
        this.decreeNumber = decreeNumber;
        this.datetimeOfFine = datetimeOfFine;
        this.placeOfFine = placeOfFine;
        this.inspectorDepartment = inspectorDepartment;
        this.inspectorFio = inspectorFio;
        this.carMark = carMark;
        this.carModel = carModel;
        this.stateSign = stateSign;
        this.vin = vin;
        this.bodyNumber = bodyNumber;
        this.motorNumber = motorNumber;
        this.gibddNumber = gibddNumber;
        this.articleNumber = articleNumber;
        this.residentAt = residentAt;
        this.phonenumber = phonenumber;
        this.workAt = workAt;
        this.workPhonenumber = workPhonenumber;
        this.fineSum = fineSum;
        this.status = status;
    }

    public Fine(entity.Fine fine){
        this(fine.getId(), fine.getDecreeNumber(), ConvertUtil.convertDateToXMLGregorianCalendar(fine.getDatetimeOfFine()),
                fine.getPlaceOfFine(), fine.getInspectorDepartment(), fine.getInspectorFio(), fine.getCarMark(), fine.getCarModel(),
                fine.getStateSign(), fine.getVin(), fine.getBodyNumber(), fine.getMotorNumber(), fine.getGibddNumber(),
                fine.getArticleNumber(), fine.getResidentAt(), fine.getPhonenumber(), fine.getWorkAt(), fine.getWorkPhonenumber(),
                fine.getFineSum(), fine.getStatus());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDecreeNumber() {
        return decreeNumber;
    }

    public void setDecreeNumber(String decreeNumber) {
        this.decreeNumber = decreeNumber;
    }

    public XMLGregorianCalendar getDatetimeOfFine() {
        return datetimeOfFine;
    }

    public void setDatetimeOfFine(XMLGregorianCalendar datetimeOfFine) {
        this.datetimeOfFine = datetimeOfFine;
    }

    public String getPlaceOfFine() {
        return placeOfFine;
    }

    public void setPlaceOfFine(String placeOfFine) {
        this.placeOfFine = placeOfFine;
    }

    public String getInspectorDepartment() {
        return inspectorDepartment;
    }

    public void setInspectorDepartment(String inspectorDepartment) {
        this.inspectorDepartment = inspectorDepartment;
    }

    public String getInspectorFio() {
        return inspectorFio;
    }

    public void setInspectorFio(String inspectorFio) {
        this.inspectorFio = inspectorFio;
    }

    public String getCarMark() {
        return carMark;
    }

    public void setCarMark(String carMark) {
        this.carMark = carMark;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getStateSign() {
        return stateSign;
    }

    public void setStateSign(String stateSign) {
        this.stateSign = stateSign;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getBodyNumber() {
        return bodyNumber;
    }

    public void setBodyNumber(String bodyNumber) {
        this.bodyNumber = bodyNumber;
    }

    public String getMotorNumber() {
        return motorNumber;
    }

    public void setMotorNumber(String motorNumber) {
        this.motorNumber = motorNumber;
    }

    public String getGibddNumber() {
        return gibddNumber;
    }

    public void setGibddNumber(String gibddNumber) {
        this.gibddNumber = gibddNumber;
    }

    public String getArticleNumber() {
        return articleNumber;
    }

    public void setArticleNumber(String articleNumber) {
        this.articleNumber = articleNumber;
    }

    public String getResidentAt() {
        return residentAt;
    }

    public void setResidentAt(String residentAt) {
        this.residentAt = residentAt;
    }

    public Integer getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(Integer phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getWorkAt() {
        return workAt;
    }

    public void setWorkAt(String workAt) {
        this.workAt = workAt;
    }

    public Integer getWorkPhonenumber() {
        return workPhonenumber;
    }

    public void setWorkPhonenumber(Integer workPhonenumber) {
        this.workPhonenumber = workPhonenumber;
    }

    public Integer getFineSum() {
        return fineSum;
    }

    public void setFineSum(Integer fineSum) {
        this.fineSum = fineSum;
    }

    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }
}
