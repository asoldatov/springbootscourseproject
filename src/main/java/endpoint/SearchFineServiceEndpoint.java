package endpoint;

import client.FineDBCallServiceInnerClient;
import model.*;
import model.searchfineservice.SearchFineByDecreeNumberRequest;
import model.searchfineservice.SearchFineByDecreeNumberResponse;
import model.searchfineservice.SearchFineServiceRequest;
import model.searchfineservice.SearchFineServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class SearchFineServiceEndpoint {

    private static final String NAMESPACE_URI = "http://searchFineService";

    private FineDBCallServiceInnerClient fineDBCallServiceInnerClient;


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "searchFineServiceRequest")
    @ResponsePayload
    public SearchFineServiceResponse searchFine(@RequestPayload SearchFineServiceRequest request) {
        SearchFineServiceResponse response = new SearchFineServiceResponse();
        List<model.Fine> fineList = new ArrayList<model.Fine>();
        List<Integer> fineIdList = new ArrayList<Integer>();
        if (request.getDriverData() != null && (request.getDriverData().getSeries() != null || request.getDriverData().getNumber() != null) &&
                (!request.getDriverData().getSeries().trim().isEmpty() || request.getDriverData().getNumber() != 0)){
            fineList = fineDBCallServiceInnerClient.callGetFinesFromDBByDriverData(new DriverData(request.getDriverData()));
        } else if (request.getVIN() != null && !request.getVIN().trim().isEmpty()){
            fineList = fineDBCallServiceInnerClient.callGetFinesFromDBByVIN(request.getVIN());
        } else if (request.getDecreeNumber() != null && !request.getDecreeNumber().trim().isEmpty()){
            fineList = fineDBCallServiceInnerClient.callGetFinesFromDBByDecreeNumber(request.getDecreeNumber());
        }
        for (model.Fine fine : fineList)
            fineIdList.add(fine.getId());
        response.setFineId(fineIdList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "searchFineByDecreeNumberRequest")
    @ResponsePayload
    public SearchFineByDecreeNumberResponse searchFineByDecreeNumber(@RequestPayload SearchFineByDecreeNumberRequest request){
        SearchFineByDecreeNumberResponse response = new SearchFineByDecreeNumberResponse();
        List<model.Fine> fineList = fineDBCallServiceInnerClient.callGetFinesFromDBByDecreeNumber(request.getDecreeNumber());
        if (fineList.size() >= 1)
            response.setFineId(fineList.get(0).getId());
        return response;
    }

    @Autowired
    public void setFineDBCallServiceInnerClient(FineDBCallServiceInnerClient fineDBCallServiceInnerClient) {
        this.fineDBCallServiceInnerClient = fineDBCallServiceInnerClient;
    }

}
