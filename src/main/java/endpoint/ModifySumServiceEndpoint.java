package endpoint;

import entity.Fine;
import model.modifysumservice.CheckFineSumRequest;
import model.modifysumservice.CheckFineSumResponse;
import model.modifysumservice.ModifySumRequest;
import model.modifysumservice.ModifySumResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import service.FineService;

@Endpoint
public class ModifySumServiceEndpoint {

    private static final String NAMESPACE_URI = "http://modifySumService";

    private FineService fineService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "modifySumRequest")
    @ResponsePayload
    public ModifySumResponse modifySum(@RequestPayload ModifySumRequest request){
        ModifySumResponse response = new ModifySumResponse();
        model.Fine modelFine = fineService.recalculateFineSumWithSomeCoefficient(request.getFineId(), request.getCoefficient());
        response.setSum(modelFine.getFineSum());
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "checkFineSumRequest")
    @ResponsePayload
    public CheckFineSumResponse checkFineSum(@RequestPayload CheckFineSumRequest request){
        CheckFineSumResponse response = new CheckFineSumResponse();
        Double coefficient = fineService.calculateCoefficientForFine(request.getFineId());
        response.setCoefficient(coefficient);
        return response;
    }

    @Autowired
    public void setFineService(FineService fineService) {
        this.fineService = fineService;
    }
}
