package endpoint;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import service.FineService;

import java.util.List;

@Endpoint
public class FineDBCallServiceEndpoint {

    private static final String NAMESPACE_URI = "http://fineDBCallService";

    private FineService fineService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getFinesFromDBRequest")
    @ResponsePayload
    public GetFinesFromDBResponse getFinesFromDB(@RequestPayload GetFinesFromDBRequest request){
        GetFinesFromDBResponse response = new GetFinesFromDBResponse();
        List<model.Fine> fineList = fineService.createXMLResponseForDriverFines(request.getDriverId().intValue());
        response.setFine(fineList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateFineInDBRequest")
    @ResponsePayload
    public UpdateFineInDBResponse updateFineInDB(@RequestPayload UpdateFineInDBRequest request){
        UpdateFineInDBResponse response = new UpdateFineInDBResponse();
        Fine updatedFine = fineService.updateFine(request.getFine());
        response.setFine(updatedFine);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getFinesFromDBByDriverDataRequest")
    @ResponsePayload
    public GetFinesFromDBByDriverDataResponse getFinesFromDBByDriverData(@RequestPayload GetFinesFromDBByDriverDataRequest request){
        GetFinesFromDBByDriverDataResponse response = new GetFinesFromDBByDriverDataResponse();
        List<model.Fine> fineList = fineService.findFinesByDriverData(request.getDriverData());
        response.setFine(fineList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getFinesFromDBByVINRequest")
    @ResponsePayload
    public GetFinesFromDBByVINResponse getFinesFromDBByVIN(@RequestPayload GetFinesFromDBByVINRequest request){
        GetFinesFromDBByVINResponse response = new GetFinesFromDBByVINResponse();
        List<model.Fine> fineList = fineService.findFinesByVIN(request.getVin());
        response.setFine(fineList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getFinesFromDBByDecreeNumberRequest")
    @ResponsePayload
    public GetFinesFromDBByDecreeNumberResponse getFinesFromDBByDecreeNumber(@RequestPayload GetFinesFromDBByDecreeNumberRequest request){
        GetFinesFromDBByDecreeNumberResponse response = new GetFinesFromDBByDecreeNumberResponse();
        List<model.Fine> fineList = fineService.findFinesByDecreeNumber(request.getDecreeNumber());
        response.setFine(fineList);
        return response;
    }

    @Autowired
    public void setFineService(FineService fineService) {
        this.fineService = fineService;
    }
}
