package endpoint;

import client.BankDBCallServiceInnerClient;
import model.payfineservice.PayFineServiceRequest;
import model.payfineservice.PayFineServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import service.FineService;

@Endpoint
public class PayFineServiceEndpoint {

    private static final String NAMESPACE_URI = "http://payFineService";

    private FineService fineService;
    private BankDBCallServiceInnerClient bankDBCallServiceInnerClient;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "payFineServiceRequest")
    @ResponsePayload
    public PayFineServiceResponse payFine(@RequestPayload PayFineServiceRequest request){
        PayFineServiceResponse response = new PayFineServiceResponse();
        entity.Fine entityFine = fineService.findFineById(request.getFineId());
        if (entityFine == null)
            throw new RuntimeException("Штраф с ID = " + request.getFineId() + " не найден");
        //entityFine.setFineSum(request.getSum());
        entityFine.setPaySum(request.getSum());
        model.BankAccount modelBankAccount = new model.BankAccount(entityFine.getDriver().getBankAccount());
        modelBankAccount = bankDBCallServiceInnerClient.callDebit(modelBankAccount, entityFine.getPaySum());
        fineService.changeStatusOfFineToPaid(entityFine);
        response.setBalance(new Long(modelBankAccount.getBalance()).intValue());
        return response;
    }

    @Autowired
    public void setFineService(FineService fineService) {
        this.fineService = fineService;
    }

    @Autowired
    public void setBankDBCallServiceInnerClient(BankDBCallServiceInnerClient bankDBCallServiceInnerClient) {
        this.bankDBCallServiceInnerClient = bankDBCallServiceInnerClient;
    }
}
