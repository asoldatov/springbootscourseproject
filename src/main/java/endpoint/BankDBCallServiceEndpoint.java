package endpoint;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import service.BankService;

@Endpoint
public class BankDBCallServiceEndpoint {

    private static final String NAMESPACE_URI = "http://bankDBCallService";

    private BankService bankService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBankAccountFromDBRequest")
    @ResponsePayload
    public GetBankAccountFromDBResponse getBankAccountFromDB(@RequestPayload GetBankAccountFromDBRequest request){
        GetBankAccountFromDBResponse response = new GetBankAccountFromDBResponse();
        model.BankAccount bankAccount = bankService.createXMLResponseForDriverBankAccount(request.getDriverId());
        response.setBankAccount(bankAccount);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateBankAccountDBRequest")
    @ResponsePayload
    public UpdateBankAccountDBResponse updateBankAccountInDB(@RequestPayload UpdateBankAccountDBRequest request){
        UpdateBankAccountDBResponse response = new UpdateBankAccountDBResponse();
        model.BankAccount bankAccount = bankService.updateBankAccount(request.getBankAccount());
        response.setBankAccount(bankAccount);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "debitRequest")
    @ResponsePayload
    public DebitResponse debit(@RequestPayload DebitRequest request){
        DebitResponse response = new DebitResponse();
        model.BankAccount bankAccount = bankService.debit(request.getBankAccount(), request.getSum());
        response.setBankAccount(bankAccount);
        return response;
    }

    @Autowired
    public void setBankService(BankService bankService) {
        this.bankService = bankService;
    }
}
