package service;

import entity.*;
import entity.Fine;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.DriverLicenseRepository;
import repository.DriverRepository;
import repository.FineRepository;
import repository.RegistrationCertificateRepository;
import util.ConvertUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FineService {

    //Количество дней для уменьшения суммы штрафа на 50% и увеличения на 100%
    public static final Integer FIFTY_PERCENT_DISCOUNT_DAYS = 20;
    public static final Double FIFTY_PERCENT_DAYS_COEFFICIENT = 0.5;
    public static final Integer DOUBLE_MARKUP_DAYS = 60;
    public static final Double DOUBLE_MARKUP_DAYS_COEFFICIENT = 2.0;
    public static final Double COMMON_COEFFICIENT = 1.0;

    //Оплачено или нет
    public static final byte IS_PAID = 1;
    public static final byte IS_NOT_PAID = 0;

    private DriverRepository driverRepository;
    private FineRepository fineRepository;
    private DriverLicenseRepository driverLicenseRepository;
    private RegistrationCertificateRepository registrationCertificateRepository;
    private BankService bankService;

    public List<model.Fine> createXMLResponseForDriverFines(int driverId){
        List<model.Fine> fineList = new ArrayList<model.Fine>();
        Driver driver = driverRepository.findOne(driverId);
        if (driver == null)
            throw new RuntimeException("Водитель с ID = " + driverId + " не найден");
        for (Fine driverFine : driver.getFines()){
            model.Fine modelFine = new model.Fine(driverFine);
            fineList.add(modelFine);
        }
        return fineList;
    }

    public model.Fine updateFine(model.Fine fine){
        Fine entityFine = fineRepository.findOne(fine.getId());
        if (entityFine == null)
            throw new RuntimeException("Штраф с ID = " + fine.getId() + " не найден");
        entityFine.setParametersFromModelFine(fine);
        entityFine = fineRepository.save(entityFine);
        return new model.Fine(entityFine);
    }

    public List<model.Fine> findFinesByDriverData(DriverData driverData){
        List<model.Fine> fineList = new ArrayList<model.Fine>();
        DriverLicense driverLicense = driverLicenseRepository.findDriverLicenseBySeriesAndNumber(driverData.getSeries(), driverData.getNumber());
        if (driverLicense == null)
            throw new RuntimeException("Водительское удостоворение с серией = " + driverData.getSeries() + " и номером = " + driverData.getNumber() + " не найдено");
        for (Fine entityFine : driverLicense.getDriver().getFines())
            if (entityFine.getStatus() == IS_NOT_PAID)
                fineList.add(new model.Fine(entityFine));
        return fineList;
    }

    public List<model.Fine> findFinesByVIN(String VIN){
        List<model.Fine> fineList = new ArrayList<model.Fine>();
        RegistrationCertificate registrationCertificate = registrationCertificateRepository.findRegistrationSertificateByVin(VIN);
        if (registrationCertificate == null)
            throw new RuntimeException("Свидетельство о ТС с идентификационным номером = " + VIN + " не найдено");
        for (Fine entityFine : registrationCertificate.getDriver().getFines())
            if (entityFine.getStatus() == IS_NOT_PAID)
                fineList.add(new model.Fine(entityFine));
        return fineList;
    }

    public List<model.Fine> findFinesByDecreeNumber(String decreeNumber){
        List<model.Fine> fineList = new ArrayList<model.Fine>();
        Fine findFine = fineRepository.findFineByDecreeNumber(decreeNumber);
        if (findFine == null)
            throw new RuntimeException("Штраф с номером постановления = " + decreeNumber + " не найден");
        if (findFine.getStatus() == IS_NOT_PAID)
            fineList.add(new model.Fine(findFine));
        return fineList;
    }

    public model.Fine recalculateFineSumWithSomeCoefficient(Integer fineId, Double coefficient){
        Fine entityFine = fineRepository.findOne(fineId);
        if (entityFine == null)
            throw new RuntimeException("Штраф с ID = " + fineId + " не найден");
        model.Fine modelFine = new model.Fine(entityFine);
        modelFine.setFineSum(new Double(modelFine.getFineSum() * coefficient).intValue());
        return modelFine;
    }

    public Double calculateCoefficientForFine(Integer fineId){
        Fine entityFine = fineRepository.findOne(fineId);
        if (entityFine == null)
            throw new RuntimeException("Штраф с ID = " + fineId + " не найден");
        Date dateTimeOfFine = entityFine.getDatetimeOfFine();
        Date today = new Date();
        int dayCount = ConvertUtil.calculateNumberOfWeekendsInRange(dateTimeOfFine, today);
        if (dayCount <= FIFTY_PERCENT_DISCOUNT_DAYS){
            return FIFTY_PERCENT_DAYS_COEFFICIENT;
        } else if (dayCount >= DOUBLE_MARKUP_DAYS){
            return DOUBLE_MARKUP_DAYS_COEFFICIENT;
        }
        return COMMON_COEFFICIENT;
    }

    public Fine findEntityFineByModelFine(model.Fine modelFine){
        Fine entityFine = fineRepository.findOne(modelFine.getId());
        if (entityFine == null)
            throw new RuntimeException("Штраф с ID = " + modelFine.getId() + " не найден");
        return entityFine;
    }

    public void changeStatusOfFineToPaid(/*model.Fine modelFine, */entity.Fine entityFine){
        entityFine.setStatus(IS_PAID);
        entityFine = fineRepository.save(entityFine);
//        modelFine.setStatus(entityFine.getStatus());
//        return modelFine;
    }

    public Fine findFineById(Integer fineId){
        return fineRepository.findOne(fineId);
    }

    @Autowired
    public void setDriverRepository(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    @Autowired
    public void setFineRepository(FineRepository fineRepository) {
        this.fineRepository = fineRepository;
    }

    @Autowired
    public void setDriverLicenseRepository(DriverLicenseRepository driverLicenseRepository) {
        this.driverLicenseRepository = driverLicenseRepository;
    }

    @Autowired
    public void setRegistrationCertificateRepository(RegistrationCertificateRepository registrationCertificateRepository) {
        this.registrationCertificateRepository = registrationCertificateRepository;
    }

    @Autowired
    public void setBankService(BankService bankService) {
        this.bankService = bankService;
    }
}
