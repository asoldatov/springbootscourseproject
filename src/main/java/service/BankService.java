package service;

import entity.BankAccount;
import entity.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.BankAccountRepository;
import repository.DriverRepository;

import java.math.BigDecimal;

@Service
public class BankService {

    private DriverRepository driverRepository;
    private BankAccountRepository bankAccountRepository;

    public model.BankAccount createXMLResponseForDriverBankAccount(Integer driverId){
        Driver findDriver = driverRepository.findOne(driverId);
        if (findDriver == null || findDriver.getBankAccount() == null)
            throw new RuntimeException("У водителя с ID = " + driverId + " не найден банковский счет");
        return new model.BankAccount(findDriver.getBankAccount());
    }

    public model.BankAccount updateBankAccount(model.BankAccount bankAccount){
        BankAccount entityBankAccount = bankAccountRepository.findOne(bankAccount.getId());
        if (entityBankAccount == null)
            throw new RuntimeException("Банковский счет с ID = " + bankAccount.getId() + " не найден");
        entityBankAccount.setParametersFromModelBankAccount(bankAccount);
        entityBankAccount = bankAccountRepository.save(entityBankAccount);
        return new model.BankAccount(entityBankAccount);
    }

    public model.BankAccount debit(model.BankAccount modelBankAccount, Integer sum){
        entity.BankAccount bankAccount = bankAccountRepository.findOne(modelBankAccount.getId());
        if (bankAccount == null)
            throw new RuntimeException("Банковский счет с ID = " + modelBankAccount.getId() + " не найден");
        if (bankAccount.getBalance().intValue() < sum)
            throw new RuntimeException("На банковском счете " + bankAccount.getAccountNumber() + " недостаточно денежных средств");
        else {
            bankAccount.setBalance(bankAccount.getBalance().subtract(new BigDecimal(sum)));
            bankAccount = bankAccountRepository.save(bankAccount);
            modelBankAccount.setBalance(bankAccount.getBalance().longValue());
        }
        return modelBankAccount;
    }

    @Autowired
    public void setDriverRepository(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    @Autowired
    public void setBankAccountRepository(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }
}
