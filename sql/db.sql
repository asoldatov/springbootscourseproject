CREATE DATABASE finedb;

USE finedb;

CREATE TABLE driver( #Водитель
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  last_name VARCHAR(30) NOT NULL, #Фамилия
  first_name VARCHAR(30) NOT NULL, #Имя
  middle_name VARCHAR(30) NOT NULL, #Отчество
  date_of_birth DATE NOT NULL, #Дата рождения
  birth_place VARCHAR(40) NOT NULL, #Место рождения
  passport_series INTEGER(4) NOT NULL, #Серия паспорта
  passport_number INTEGER(6) NOT NULL #Номер паспорта
);

CREATE TABLE driver_license( #Водительские права
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  series VARCHAR(4) NOT NULL, #Серия
  number INTEGER(6) NOT NULL, #Номер
  categories VARCHAR(10) NOT NULL, #Категории
  date_of_issue DATE NOT NULL, #Дата выдачи
  valid_until DATE NOT NULL, #Действительно до
  gibdd_code VARCHAR(4) NOT NULL, #Код гибдд
  special_notes VARCHAR(300) NOT NULL, #Особые отметки
  driver_id INT NOT NULL,
  FOREIGN KEY(driver_id) REFERENCES driver(id)
);

CREATE TABLE registration_certificate( #Свидетельство о регистрации ТС
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  register_sign VARCHAR(9) NOT NULL, #Регистрационный знак
  VIN VARCHAR(17) NOT NULL, #Идентификационный номер
  mark VARCHAR(30) NOT NULL, #Марка
  model VARCHAR(30) NOT NULL, #Модель
  category VARCHAR(3) NOT NULL, #Категория
  year_of_issue INTEGER NOT NULL, #Год выпуска
  motor_model VARCHAR(5) NOT NULL, #Модель двигателя
  motor_number VARCHAR(10) NOT NULL, #Номер двигателя
  chassis_number VARCHAR(30) NOT NULL, #Номер шасси
  body_number VARCHAR(30) NOT NULL, #Номер кузова
  color VARCHAR(30) NOT NULL, #Цвет
  engine_power VARCHAR(10) NOT NULL, #Мощность двигателя
  engine_displacement_volume INTEGER(5) NOT NULL, #Рабочий объем двигателя
  permissible_weight INTEGER(5) NOT NULL, #Разрешенная масса
  noload_weight INTEGER(5) NOT NULL, #Масса без нагрузки
  driver_id INT NOT NULL,
  FOREIGN KEY(driver_id) REFERENCES driver(id)
);

CREATE TABLE fine( #Штраф
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  decree_number VARCHAR(12) NOT NULL, #Номер постановления
  datetime_of_fine DATETIME NOT NULL, #Дата и время штрафа
  place_of_fine VARCHAR(100) NOT NULL, #Регион штрафа
  inspector_department VARCHAR(100) NOT NULL, #Поздразделение инспектора ГИБДД
  inspector_fio VARCHAR(100) NOT NULL, #ФИО инспектора ГИБДД
  car_mark VARCHAR(30) NOT NULL, #Марка автомобиля
  car_model VARCHAR(30) NOT NULL, #Модель автомобиля
  state_sign VARCHAR(6) NOT NULL, #Государственный регистрационный знак
  VIN VARCHAR(17) NOT NULL, #Идентификационный номер
  body_number VARCHAR(30) NOT NULL, #Номер кузова
  motor_number VARCHAR(10) NOT NULL, #Номер двигателя
  gibdd_number VARCHAR(10) NOT NULL, #Номер водителя ГИБДД
  article_number VARCHAR(10) NOT NULL, #Номер статьи
  resident_at VARCHAR(200) NOT NULL, #Проживающего по адресу
  phonenumber INTEGER(11) NOT NULL, #Номер телефона
  work_at VARCHAR(200) NOT NULL, #Работающего
  work_phonenumber INTEGER(11) NOT NULL, #Рабочий номер телефона
  fine_sum INT NOT NULL, #Сумма штрафа
  status TINYINT(1) NOT NULL, #Статус штрафа
  driver_id INT NOT NULL,
  FOREIGN KEY(driver_id) REFERENCES driver(id)
);

CREATE TABLE bank_account( #Банковский счет
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  bank_id INTEGER NOT NULL, #ID банка заглушка
  account_number VARCHAR(40) NOT NULL, #Номер счета
  balance NUMERIC(21) NOT NULL, #Баланс на счете
  driver_id INT NOT NULL,
  FOREIGN KEY(driver_id) REFERENCES driver(id)
);

INSERT INTO `finedb`.`driver` (`id`, `last_name`, `first_name`, `middle_name`, `date_of_birth`, `birth_place`, `passport_series`, `passport_number`) VALUES ('1', 'Петров', 'Петр', 'Петрович', '1993-08-08', 'Владимир', '0404', '336336');
INSERT INTO `finedb`.`driver` (`id`, `last_name`, `first_name`, `middle_name`, `date_of_birth`, `birth_place`, `passport_series`, `passport_number`) VALUES ('2', 'Иванов', 'Иван', 'Иванович', '1993-04-04', 'Владимир', '2214', '343632');
UPDATE `finedb`.`driver` SET `passport_series`='4041' WHERE `id`='1';
INSERT INTO `finedb`.`driver` (`id`, `last_name`, `first_name`, `middle_name`, `date_of_birth`, `birth_place`, `passport_series`, `passport_number`) VALUES ('3', 'Сидоров', 'Сидор', 'Сидорович', '1993-03-03', 'Владимир', '4444', '335333');

INSERT INTO `finedb`.`bank_account` (`id`, `bank_id`, `account_number`, `balance`, `driver_id`) VALUES ('1', '2', '3344556677', '5000', '1');
INSERT INTO `finedb`.`bank_account` (`id`, `bank_id`, `account_number`, `balance`, `driver_id`) VALUES ('2', '3', '4432425353', '3500', '2');
INSERT INTO `finedb`.`bank_account` (`id`, `bank_id`, `account_number`, `balance`, `driver_id`) VALUES ('3', '1', '9988545452', '2100', '3');

INSERT INTO `finedb`.`fine` (`id`, `decree_number`, `datetime_of_fine`, `place_of_fine`, `inspector_department`, `inspector_fio`, `car_mark`, `car_model`, `state_sign`, `VIN`, `body_number`, `motor_number`, `gibdd_number`, `article_number`, `resident_at`, `phonenumber`, `work_at`, `work_phonenumber`, `fine_sum`, `status`, `driver_id`) VALUES ('1', '47AE028999', '2016-04-15 20:03:00', 'г.Владимир ул.Завадского д.11 дорога слева от дома', 'ОДД ГИБДД МВД России г.Владимир', 'Палкин Виктор Петрович', 'Kia', 'Rio', 'а046еу', 'WZ3435KXE321453XB', 'W234611', 'ATN034512', '03214355', '11.23.1', 'г.Владимир ул.Пушкина д.1 кв.1', '331253', 'Владимирский Государственный Университет', '554455', '2500', '0', '1');
INSERT INTO `finedb`.`fine` (`id`, `decree_number`, `datetime_of_fine`, `place_of_fine`, `inspector_department`, `inspector_fio`, `car_mark`, `car_model`, `state_sign`, `VIN`, `body_number`, `motor_number`, `gibdd_number`, `article_number`, `resident_at`, `phonenumber`, `work_at`, `work_phonenumber`, `fine_sum`, `status`, `driver_id`) VALUES ('2', '47AE028341', '2016-04-09 20:03:00', 'г.Владимир Пекинка 146км ', 'ОДД ГИБДД МВД России г.Владимир', 'Валенков Михаил Дмитриевич', 'Kia', 'Rio', 'а046еу', 'WZ3435KXE321453XB', 'W234611', 'ATN034512', '03214355', '12.9.2', 'г.Владимир ул.Пушкина д.1 кв.1', '331253', 'Владимирский Государственный Университет', '554455', '500', '0', '1');
INSERT INTO `finedb`.`fine` (`id`, `decree_number`, `datetime_of_fine`, `place_of_fine`, `inspector_department`, `inspector_fio`, `car_mark`, `car_model`, `state_sign`, `VIN`, `body_number`, `motor_number`, `gibdd_number`, `article_number`, `resident_at`, `phonenumber`, `work_at`, `work_phonenumber`, `fine_sum`, `status`, `driver_id`) VALUES ('3', '47АВ019214', '2016-02-01 10:14:00', 'г.Владимир Пекинка 123км', 'ОДД ГИБДД МВД России г.Владимир', 'Валенштейн Акакий Федорович', 'Volkswagen', 'Golf', 'е783ка', 'ZW2143XEX342145XB', 'W435124', 'ATN021344', '02155677', '12.5.2', 'г.Владимир ул.Добросельская д.187 кв.3', '436312', 'ООО \"Макдоналдс\"', '223433', '500', '0', '2');
INSERT INTO `finedb`.`fine` (`id`, `decree_number`, `datetime_of_fine`, `place_of_fine`, `inspector_department`, `inspector_fio`, `car_mark`, `car_model`, `state_sign`, `VIN`, `body_number`, `motor_number`, `gibdd_number`, `article_number`, `resident_at`, `phonenumber`, `work_at`, `work_phonenumber`, `fine_sum`, `status`, `driver_id`) VALUES ('4', '47АВ019692', '2016-02-08 21:30:00', 'г.Владимир Пекинка 122км', 'ОДД ГИБДД МВД России г.Владимир', 'Валенштейн Акакий Федорович', 'Volkswagen', 'Golf', 'е783ка', 'ZW2143XEX342145XB', 'W435124', 'ATN021344', '02155677', '12.9.2', 'г.Владимир ул.Добросельская д.187 кв.3', '436312', 'ООО \"Макдоналдс\"', '223433', '500', '0', '2');
INSERT INTO `finedb`.`fine` (`id`, `decree_number`, `datetime_of_fine`, `place_of_fine`, `inspector_department`, `inspector_fio`, `car_mark`, `car_model`, `state_sign`, `VIN`, `body_number`, `motor_number`, `gibdd_number`, `article_number`, `resident_at`, `phonenumber`, `work_at`, `work_phonenumber`, `fine_sum`, `status`, `driver_id`) VALUES ('5', '47АЕ023454', '2016-01-07 20:35:00', 'г.Владимир ул.Большая Московская около д.15', 'ОДД ГИБДД МВД России г.Владимир', 'Жирков Александр Петрович', 'Audi', 'A8', 'а221вв', 'ZW231FS341243536F', 'W222643', 'ATN021023', '03453124', '12.2.1', 'г.Владимир ул.Безыменского д.13 кв.44', '213563', 'ООО \"Бургер кинг\"', '343612', '500', '0', '3');

INSERT INTO `finedb`.`registration_certificate` (`id`, `register_sign`, `VIN`, `mark`, `model`, `category`, `year_of_issue`, `motor_model`, `motor_number`, `chassis_number`, `body_number`, `color`, `engine_power`, `engine_displacement_volume`, `permissible_weight`, `noload_weight`, `driver_id`) VALUES ('1', 'а046еу', 'WZ3435KXE321453XB', 'Kia', 'Rio', 'B', '2000', '-', 'ATN034512', '-', 'W234611', 'Красный', '77/105', '1577', '1650', '1300', '1');
INSERT INTO `finedb`.`registration_certificate` (`id`, `register_sign`, `VIN`, `mark`, `model`, `category`, `year_of_issue`, `motor_model`, `motor_number`, `chassis_number`, `body_number`, `color`, `engine_power`, `engine_displacement_volume`, `permissible_weight`, `noload_weight`, `driver_id`) VALUES ('2', 'о035ах', '47АВ019214', 'Volkswagen', 'Golf', 'B', '1999', '-', 'ATN213451', '-', 'W512631', 'Зеленый', '23/105', '1233', '1453', '1250', '2');
INSERT INTO `finedb`.`registration_certificate` (`id`, `register_sign`, `VIN`, `mark`, `model`, `category`, `year_of_issue`, `motor_model`, `motor_number`, `chassis_number`, `body_number`, `color`, `engine_power`, `engine_displacement_volume`, `permissible_weight`, `noload_weight`, `driver_id`) VALUES ('3', 'у994ах', '47АЕ023454', 'Audi', 'A8', 'B', '1997', '-', 'ATN234111', '-', 'W321534', 'Черный', '94/105', '1345', '1655', '1954', '3');
UPDATE `finedb`.`registration_certificate` SET `VIN`='ZW2143XEX342145XB', `engine_power`='83/105' WHERE `id`='2';

INSERT INTO `finedb`.`driver_license` (`id`, `series`, `number`, `categories`, `date_of_issue`, `valid_until`, `gibdd_code`, `special_notes`, `driver_id`) VALUES ('1', '3304', '215354', 'B', '2012-08-10', '2022-08-10', '3301', 'B', '1');
INSERT INTO `finedb`.`driver_license` (`id`, `series`, `number`, `categories`, `date_of_issue`, `valid_until`, `gibdd_code`, `special_notes`, `driver_id`) VALUES ('2', '3304', '864643', 'B', '2010-02-03', '2020-02-03', '3301', 'B', '2');
INSERT INTO `finedb`.`driver_license` (`id`, `series`, `number`, `categories`, `date_of_issue`, `valid_until`, `gibdd_code`, `special_notes`, `driver_id`) VALUES ('3', '3304', '124353', 'B', '2007-01-02', '2017-01-02', '3301', 'B', '3');

UPDATE `finedb`.`registration_certificate` SET `VIN`='WWZ34521RXY35121S' WHERE `id`='2';
UPDATE `finedb`.`registration_certificate` SET `VIN`='WX34215SGDGTRTRWA' WHERE `id`='3';

UPDATE `finedb`.`fine` SET `recalculated_sum`='2500' WHERE `id`='1';
UPDATE `finedb`.`fine` SET `recalculated_sum`='500' WHERE `id`='2';
UPDATE `finedb`.`fine` SET `recalculated_sum`='500' WHERE `id`='3';
UPDATE `finedb`.`fine` SET `recalculated_sum`='500' WHERE `id`='4';
UPDATE `finedb`.`fine` SET `recalculated_sum`='500' WHERE `id`='5';

UPDATE `finedb`.`fine` SET `datetime_of_fine`='2016-03-15 20:03:00' WHERE `id`='2';

ALTER TABLE `finedb`.`fine`
ADD COLUMN `pay_sum` INT(11) NULL AFTER `status`;

UPDATE `finedb`.`fine` SET `pay_sum`='0' WHERE `id`='1';
UPDATE `finedb`.`fine` SET `pay_sum`='0' WHERE `id`='2';
UPDATE `finedb`.`fine` SET `pay_sum`='0' WHERE `id`='3';
UPDATE `finedb`.`fine` SET `pay_sum`='0' WHERE `id`='4';
UPDATE `finedb`.`fine` SET `pay_sum`='0' WHERE `id`='5';

